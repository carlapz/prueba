class Rectangulo: 
    def __init__ (self, b, h): 
        self.base = b 
        self.altura = h 
        self.miarea = self.base * self.altura 
    def perimetro ( self):
        return self.base * 2 + self.altura * 2
    def area (self):
        return self.miarea 

Rectangulo01 = Rectangulo (3,2)
print ("El perímetro del rectangulo es:", Rectangulo01.perimetro())
print ("El área del rectangulo es:", Rectangulo01.area())
