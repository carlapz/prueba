import unittest
from mul import multiply
from mul import add

class MultiplyTestCase(unittest.TestCase):
    def test_multiplication_with_correct_values(self):
        self.assertEqual(multiply(5, 5), 25)
    def test_multiplication_with_incorrect_values (self):
        self.asserNottEqual (multiply(5, 5), 24)
    def test_add_with_correct_values (self):
        self.assertEqual (add(5, 5), 10)



unittest.main()